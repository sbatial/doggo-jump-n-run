class Charak {
int body_colour;
int height;
int width;
int animX;
int animY;

Charak(int body_colour,int animX,int animY, int width,int height){
        this.body_colour = body_colour;
        this.height = height;
        this.width = width;
        this.animX = animX;
        this.animY = animY;
}

void showAnim(){
        noStroke();
        fill(0xff00ffff);
        rect(animX,animY,width,height);
}

int getX(){
        return animX;
}
int getY(){
        return this.animY;
}
void setX(int var){
        this.animX=var;
}
void setY(int var){
        this.animY=var;
}
int getHeight(){
        return height;
}

}
